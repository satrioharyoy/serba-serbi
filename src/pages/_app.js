import { Layout } from "@app/components/templates";
import { NewsDetailProvider } from "@app/context/useDetailNews";
import "@app/styles/globals.css";

export default function MyApp({ Component, pageProps }) {
  return (
    <NewsDetailProvider>
      <Layout>
        <Component {...pageProps} />
      </Layout>
    </NewsDetailProvider>
  );
}
