import {
  NewsDetailContext,
  NewsDetailProvider,
} from "@app/context/useDetailNews";
import { getListArticles } from "@app/services/articles";
import { message } from "antd";
import { useRouter } from "next/router";
import { useCallback, useContext, useEffect, useState } from "react";
import ViewArticles from "./view";

function ListArticles() {
  const [params, setParams] = useState({
    page: null,
    size: 6,
    full_content: 1,
    q: null,
  });
  const [dataArticles, setDataArticles] = useState(null);
  const [isLoading, setIsLoading] = useState(false);
  const [isError, setIsError] = useState(false);
  const router = useRouter();
  const { setDetailNews } = useContext(NewsDetailContext);
  const [keyword, setKeyword] = useState(null);
  const [currentPage, setCurrentPage] = useState(1);

  const onPressReadMore = (detailNews) => {
    setDetailNews(detailNews);
    router.push(`/detail-article/${detailNews?.article_id}`);
  };

  const fetchData = useCallback(async () => {
    setIsLoading(true);
    setIsError(false);
    const articles = await getListArticles(params);
    if (articles.error) {
      console.log("check articles => ", articles);
      setIsError(true);
      message.error(`Error get article list ${articles.message}`);
      setIsLoading(false);
    }
    if (articles.success) {
      setDataArticles(articles ?? []);
      setIsLoading(false);
    }
  }, [params]);

  useEffect(() => {
    fetchData();
  }, [fetchData]);

  return (
    <NewsDetailProvider>
      <ViewArticles
        articles={dataArticles}
        onChangePage={(page) => {
          setCurrentPage(page),
            setParams({
              ...params,
              page: page === 1 ? null : dataArticles?.nextPage,
            });
        }}
        isLoading={isLoading}
        isError={isError}
        onPressReadMore={(detail) => onPressReadMore(detail)}
        onSetKeyword={(keyword) => setKeyword(keyword)}
        onSearchAction={() =>
          setParams({ ...params, q: keyword === "" ? null : keyword })
        }
        currentPage={currentPage}
      />
    </NewsDetailProvider>
  );
}

export default ListArticles;
