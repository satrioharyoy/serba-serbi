import { SearchOutlined } from "@ant-design/icons";
import { CardArticle, PaginationArticle } from "@app/components/molecules";
import { Input, Result, Skeleton } from "antd";
import styles from "./style.module.css";

function ViewArticles({
  articles,
  onChangePage,
  isLoading,
  isError,
  onPressReadMore,
  onSetKeyword,
  onSearchAction,
  currentPage,
}) {
  const onClickReadMore = (article) => {
    onPressReadMore(article);
  };

  return (
    <div className={styles.container}>
      <div className={styles.titlePageContainer}>
        <h2 className={styles.titlePageText}>Article</h2>
        <Input
          className={styles.searchContainer}
          placeholder="Cari"
          prefix={
            <SearchOutlined
              onClick={onSearchAction}
              className="h-24 text-lg mr-2 font-extrabold cursor-pointer"
            />
          }
          onChange={(e) => onSetKeyword(e.target.value)}
          onKeyDown={(e) => {
            if (e.key === "Enter") {
              onSearchAction();
            }
          }}
        />
      </div>
      <div className="grid grid-cols-2 xl:gap-x-24 gap-y-12 ">
        {!isLoading && articles?.results?.length > 0
          ? articles.results.map((articles) => {
              return (
                <CardArticle
                  key={`${articles?.title}`}
                  title={articles?.title ?? ""}
                  urlToimage={articles?.image_url ?? ""}
                  desc={articles?.description ?? ""}
                  onPressReadMore={() => onClickReadMore(articles)}
                  publishedDate={articles?.pubDate ?? ""}
                />
              );
            })
          : null}

        {isLoading && (
          <>
            <Skeleton active />
            <Skeleton active />
            <Skeleton active />
            <Skeleton active />
          </>
        )}
      </div>
      {isError && (
        <div className="justify-center ">
          <Result
            status="error"
            title={"Error"}
            subTitle="Something went wrong."
          />
        </div>
      )}
      {!isError && (
        <PaginationArticle
          currentPage={currentPage}
          totalArticle={10}
          onChange={onChangePage}
        />
      )}
    </div>
  );
}

export default ViewArticles;
