import { ShareIcon } from "@app/assets/icons";
import moment from "moment";
import Image from "next/image";
import styles from "./style.module.css";
import { Breadcrumb } from "antd";
import Link from "next/link";
import { CardArticle } from "@app/components/molecules";

function DetailArticle({ detailNews, dataArticles, isLoading, isError }) {
  return (
    <div className={styles.container}>
      <Breadcrumb
        items={[
          {
            title: (
              <Link href={"/articles"}>
                <p className="text-primary">Article</p>
              </Link>
            ),
          },
          { title: `${detailNews?.title}` },
        ]}
      />
      <h2 className={styles.titleText}>{detailNews?.title}</h2>
      <div className={styles.publishDateContainer}>
        <p className={styles.publishDate}>
          Dipost Oleh{" "}
          {detailNews?.creator?.length > 0 ? detailNews?.creator[0] : null} -{" "}
          {moment(detailNews?.pubDate).format("DD MMMM YYYY HH:mm")}
        </p>
        <Image alt="share-icon" src={ShareIcon} />
      </div>
      <div className={styles.contentContainer}>
        <div className={styles.image}>
          <Image
            fill
            objectFit="cover"
            alt="image-news"
            src={detailNews?.image_url}
          />
        </div>
        <p
          className={styles.content}
          dangerouslySetInnerHTML={{ __html: detailNews?.content }}
        />
        <p className="text-grayscale mb-2">Tag</p>
        <div className="flex flex-row gap-x-2">
          {detailNews?.category?.length > 0 &&
            detailNews?.category?.map((category) => {
              return <div key={category}>{`#${category}`}</div>;
            })}
        </div>
        <p className="text-grayscale mt-10 mb-2">Keyword</p>
        <div className="flex flex-wrap flex-row gap-x-5 gap-y-2">
          {detailNews?.keywords?.length > 0 &&
            detailNews?.keywords?.map((keywords) => {
              return <div key={keywords}>{`${keywords}`}</div>;
            })}
        </div>
        <h3 className={styles.similarText}>Similar</h3>
        <div className="grid grid-cols-2 xl:gap-x-24 gap-y-10 items-center">
          {!isLoading && dataArticles?.results?.length > 0
            ? dataArticles.results.map((dataArticles) => {
                return (
                  <CardArticle
                    key={`${dataArticles?.title}`}
                    title={dataArticles?.title ?? ""}
                    urlToimage={dataArticles?.image_url ?? ""}
                    desc={dataArticles?.description ?? ""}
                    onPressReadMore={() => onClickReadMore(dataArticles)}
                    publishedDate={dataArticles?.pubDate ?? ""}
                  />
                );
              })
            : null}
        </div>
      </div>
    </div>
  );
}

export default DetailArticle;
