import { NewsDetailContext } from "@app/context/useDetailNews";
import { useCallback, useContext, useEffect, useState } from "react";
import DetailArticle from "./view";
import { getListArticlesSimilar } from "@app/services/articles";

function ArticleDetail() {
  const { detailNews } = useContext(NewsDetailContext);
  const [dataArticles, setDataArticles] = useState(null);
  const [isLoading, setIsLoading] = useState(false);
  const [isError, setIsError] = useState(false);

  const fetchData = useCallback(async () => {
    if (detailNews?.category?.length > 0) {
      setIsLoading(true);
      setIsError(false);
      const articles = await getListArticlesSimilar(detailNews?.category[0]);
      if (articles.error) {
        setIsError(true);
        message.error(`Error get article list ${articles.message}`);
        setIsLoading(false);
      }
      if (articles.success) {
        setDataArticles(articles ?? []);
        setIsLoading(false);
      }
    }
  }, [detailNews]);

  useEffect(() => {
    fetchData();
  }, [fetchData]);

  return (
    <DetailArticle
      detailNews={detailNews}
      isLoading={isLoading}
      isError={isError}
      dataArticles={dataArticles}
    />
  );
}

export default ArticleDetail;
