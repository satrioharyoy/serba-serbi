import React from "react";

function Home() {
  return (
    <div className="text-center font-bold text-lg">
      Welcome to serba - serbi
    </div>
  );
}

export default Home;
