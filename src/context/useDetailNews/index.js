import { createContext, useContext, useState } from "react";

export const NewsDetailContext = createContext();

export function NewsDetailProvider({ children }) {
  const [detailNews, setDetailNews] = useState({
    source: {
      id: "nbc-news",
      name: "NBC News",
    },
    author: "Dareh Gregorian, Jonathan Dienst",
    title:
      "Sen. Bob Menendez to be arraigned as resignation calls grow louder - NBC News",
    description:
      "The New Jersey Democrat is set to appear before a federal judge in New York City on Wednesday morning alongside his wife Nadine, who was also charged in the indictment.",
    url: "https://www.nbcnews.com/politics/congress/sen-bob-menendez-arraigned-resignation-calls-grow-louder-rcna117438",
    urlToImage:
      "https://media-cldnry.s-nbcnews.com/image/upload/t_nbcnews-fp-1200-630,f_auto,q_auto:best/rockcms/2023-09/230927-bob-menendez-al-1007-318ff7.jpg",
    publishedAt: "2023-09-27T14:20:23Z",
    content: `<div>
      <h2>What is Lorem Ipsum?</h2>
      <p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
      </div>`,
  });

  return (
    <NewsDetailContext.Provider value={{ detailNews, setDetailNews }}>
      {children}
    </NewsDetailContext.Provider>
  );
}
