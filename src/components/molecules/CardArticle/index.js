import moment from "moment";
import Image from "next/image";
import Link from "next/link";
import styles from "./style.module.css";

function CardArticle({
  urlToimage,
  title,
  desc,
  publishedDate,
  onPressReadMore,
}) {
  return (
    <div className="w-[551px]">
      <div className="h-64 w-[551px] relative">
        <Image
          className="rounded-md"
          alt="image-article"
          src={urlToimage}
          objectFit="fill"
          fill
        />
      </div>
      <h3 className={styles.articleTitle}>{title}</h3>
      <p className={styles.articleDescription}>
        {moment(publishedDate).format("DD MMMM YYYY")}
      </p>
      <p className={styles.articleDescription}>{desc}</p>
      <p
        className={styles.buttonReadmore}
        alt="read more"
        onClick={onPressReadMore}
      >
        Read More
      </p>
    </div>
  );
}

export default CardArticle;
