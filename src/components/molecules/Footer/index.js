import {
  FacebookIcon,
  InstagramIcon,
  SerbaSerbiLong,
  YoutubeIcon,
} from "@app/assets/icons";
import { Divider } from "antd";
import Image from "next/image";
import Link from "next/link";
import React from "react";
import styles from "./style.module.css";

function Footer() {
  return (
    <footer className={styles.container}>
      <div className={styles.menuContainer}>
        <div className={styles.menuSection}>
          <Image alt="icon" src={SerbaSerbiLong} />
          <div className={styles.listContainer}>
            <h5 className={styles.menuTitle}>Menu</h5>
            <ul className={styles.ul}>
              <li>
                <Link href={"home"} className={styles.menuList}>
                  Home
                </Link>
              </li>
            </ul>
          </div>

          <div className={styles.listContainer}>
            <h5 className={styles.menuTitle}>Bantuan</h5>
            <ul className={styles.ul}>
              <li>
                <Link href={"/home"} className={styles.menuList}>
                  Pusat Bantuan
                </Link>
              </li>
              <li>
                <Link href={"/home"} className={styles.menuList}>
                  Privacy Policy
                </Link>
              </li>
            </ul>
          </div>
        </div>
      </div>
      <Divider />
      <div className={styles.socmedContainer}>
        <div className={styles.copyrightText}>
          © 2022 Sislog. All Rights Reserved.
        </div>
        <ul className={styles.socmedUl}>
          <li>
            <Image alt="ic_facebook" src={FacebookIcon} />
          </li>
          <li>
            <Image alt="ic_youtube" src={YoutubeIcon} />
          </li>
          <li>
            <Image alt="ic_instagram" src={InstagramIcon} />
          </li>
        </ul>
      </div>
    </footer>
  );
}

export default Footer;
