import { Pagination } from "antd";
import React from "react";
import styles from "./style.module.css";

function PaginationArticle({ currentPage, totalArticle, onChange }) {
  const itemRender = (_, type, originalElement) => {
    if (type === "prev") {
      return <span className="text-primary font-medium ">Prev</span>;
    }
    if (type === "next") {
      return <span className="text-primary">Next</span>;
    }
    if (type === "page" && _ !== currentPage) {
      return <span className="text-primary">{_}</span>;
    }
    if (type === "page" && _ === currentPage) {
      return (
        <div className=" active:border-primary border border-primary hover:bg-red-100 text-primary rounded-md z-50 ">
          {_}
        </div>
      );
    }
    return originalElement;
  };

  return (
    <Pagination
      className={styles.container}
      defaultPageSize={6}
      itemRender={itemRender}
      total={totalArticle}
      showSizeChanger={false}
      jumpNextIcon={false}
      jumpPrevIcon={false}
      current={currentPage}
      onChange={onChange}
    />
  );
}

export default PaginationArticle;
