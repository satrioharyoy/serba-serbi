import { Footer, Navbar } from "@app/components/molecules";
import { useRouter } from "next/router";
import { useEffect } from "react";
import { Poppins } from "@next/font/google";

const poppins = Poppins({
  subsets: ["latin"],
  variable: "--font-poppins",
  weight: ["100", "200", "300", "400", "500", "600", "700", "800", "900"],
});

export default function Layout({ children }) {
  const router = useRouter();

  useEffect(() => {
    // detect auth

    // redirect from index
    if (router?.pathname === "/") {
      router?.push("/home");
    }
  }, [router]);

  return (
    <div
      className={`${poppins.variable} font-poppins flex flex-col h-screen 2xl:px-72 xl:px-32`}
    >
      <Navbar />
      <main className="flex-1">{children}</main>
      <Footer />
    </div>
  );
}
