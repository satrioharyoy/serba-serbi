export { default as SerbaSerbiIcon } from "./serba_serbi_icon.svg";
export { default as SerbaSerbiLong } from "./serba_serbi_long.svg";
export { default as FacebookIcon } from "./ic_facebook.svg";
export { default as YoutubeIcon } from "./ic_youtube.svg";
export { default as InstagramIcon } from "./ic_instagram.svg";
export { default as ShareIcon } from "./share_icon.svg";
