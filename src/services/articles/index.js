import { api } from "../api";

export const getListArticles = async (value) => {
  try {
    const res = await api.get("/news", {
      params: { ...value, country: "us" },
    });
    const successResponse = {
      success: true,
      ...res.data,
    };
    return successResponse;
  } catch (error) {
    const responseError = {
      error: true,
      ...error.response.data.results,
    };
    return responseError;
  }
};

export const getListArticlesSimilar = async (value) => {
  try {
    const res = await api.get("/news", {
      params: { size: 2, full_content: 1, country: "us", q: value },
    });

    const successResponse = {
      success: true,
      ...res.data,
    };
    return successResponse;
  } catch (error) {
    const responseError = {
      error: true,
      ...error.response.data.results,
    };
    return responseError;
  }
};
