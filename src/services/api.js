import axios from "axios";

export const api = axios.create({
  baseURL: "https://newsdata.io/api/1/",
  timeout: 30000,
  params: {
    apikey: "pub_302537b7a32770dd7535adbabd8fc541c639d",
  },
});
